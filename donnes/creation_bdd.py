import sqlite3


def connection(nom_dbb):
    conn = sqlite3.connect(f"donnes/{nom_dbb}.db")
    return conn


def deconnection(conn):
    conn.close()


def creation_tables(conn):
    cur = conn.cursor()
    cur.execute('''
        CREATE TABLE IF NOT EXISTS pays (
            pays_id INTEGER PRIMARY KEY,
            pays_nom VARCHAR(13)
            )
            ''')

    cur.execute('''
        CREATE TABLE IF NOT EXISTS annees (
            an_id INTEGER PRIMARY KEY,
            an_valeur INTEGER
        )
        ''')

    cur.execute('''
        CREATE TABLE IF NOT EXISTS nucleaire (
        nuc_id INTEGER PRIMARY KEY,
        nuc_pays_id INTEGER,
        nuc_an_id INTEGER,
        nuc_val REAL,
        FOREIGN KEY (nuc_pays_id) REFERENCES pays(pays_id),
        FOREIGN KEY (nuc_an_id) REFERENCES annees(an_id)
        )
        ''')

    conn.commit()


def insert_pays(df, cur):
    for a in df.index:
        cur.execute('''
            INSERT INTO main.pays(pays_nom) VALUES (?)''',
                    (a,))


def insert_an(df, cur):
    for p in df:
        cur.execute('''
            INSERT INTO main.annees(an_valeur) VALUES (?)''',
                    (p,))


def insertion_initiale_valeur(df, cur, pays, pays_id):
    a = 0
    for v in df[pays]:
        a = a + 1
        cur.execute('''
            INSERT INTO main.nucleaire(nuc_an_id,nuc_pays_id, nuc_val) VALUES (?,?,?)''',
                    (a, pays_id, v))


def verif_avant_insert(cur):
    cur.execute("SELECT COUNT(*) FROM pays;")
    nb = cur.fetchone()[0]
    if nb == 0:
        return True
    else:
        return False


def insert_tout(cur, df):
    go = verif_avant_insert(cur)
    if go:
        insert_pays(df, cur)
        insert_an(df, cur)
        insertion_initiale_valeur(df, cur, 'france', 1)
        insertion_initiale_valeur(df, cur, 'germany', 2)
        insertion_initiale_valeur(df, cur, 'japan', 3)
        print("Donné initiale inséré")
    else:
        print("Resalut, la Base de Données est toujours présente")


def table_log():
    conn = connection('nuc')
    cur = conn.cursor()
    cur.execute('''
        CREATE TABLE IF NOT EXISTS log (
            log_id INTEGER PRIMARY KEY,
            log_pays_id INTEGER,
            log_valeur_predite REAL,
            log_contexte TEXT)
        ''')
    conn.commit()


def ajout_log(pays_id, pred, texte):
    conn = sqlite3.connect('donnes/nuc.db')
    cur = conn.cursor()
    cur.execute('''
        INSERT INTO log(log_pays_id, log_valeur_predite, log_contexte) VALUES (?,?,?)''',
                (pays_id, pred, texte))
    conn.commit()
    conn.close()
