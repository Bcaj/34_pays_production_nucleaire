# Prédire
Avant de passer les valeurs dans une fonction qui va prédire une valeur pour un x donnée, où x est une année.  
Il faut déjà respecter les règles de l'apprentissage machine : 
- mettre les données sur la même échelle
  - standardisation : moyenne a 0 et écart type à 1
  - normalisation : minimum a 0 et maximum à 1 

- vérifier la prédiction en coupant le jeu de donnée en 2 :
  - un jeu d'entrainement : ~80% des valeurs du jeu de donnée
  - un jeu de test : le reste qui va être utilisé pour vérifier la prédiction et calculer les scores

- entrainer le modèle pour obtenir les paramètres
- prédire
- vérifier
- obtenir les scores du modèles

## Vérification
En fonction des cas, il y a des améliorations à faire suite au premier test. 
- augmentation des données, créé pour faire en sorte d'avoir plus de ligne de test, la difficulté avec les données temporelles est qu'il doit y avoir une continuité avec les index
- ajout de variable
  - création à partir des existantes
  - ajout depuis d'autres sources
- changer les hyperparamètres du modèle
- faire une validation croisée sur les données et les hyperparamètres
- vérifier les scores obtenus et estimer si une autre itération est encore nécessaire pour améliorer les performances du modèle

- changer d'hypothèse de sélection du modèle
  - passer d'une droite à une polynomiale 
  - un cercle de classification pour séparer les données
  - réseau de neuronnes performant 
  
