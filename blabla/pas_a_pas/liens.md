lieu et puissance des centrales nucléaire <br>
https://opendata.edf.fr/explore/dataset/centrales-de-production-nucleaire-edf/table/?disjunctive.centrale&disjunctive.tranche&disjunctive.sous_filiere&sort=-tri

<br>

origine de l'énergie fournie par EDF <br>
https://opendata.edf.fr/explore/dataset/origine-de-lelectricite-fournie-par-edf-sa/information/?disjunctive.categorie&disjunctive.sous_categorie&sort=-tri
<br>

historique des accidents nucléaire dans le monde <br>
https://www.notre-environnement.gouv.fr/ree/rapport-sur-l-etat-de-l-environnement/themes-ree/risques-nuisances-pollutions/risques-technologiques/risque-nucleaire/article/le-classement-des-incidents-et-accidents-nucleaires
<br>

datagouv : 
production nationale annuelle par filière 2012 2022 <br>
https://www.data.gouv.fr/fr/datasets/production-nationale-annuelle-par-filiere-2012-a-2021/
<br>

origine de l'électricité fournie par EDF   <br>
https://www.data.gouv.fr/fr/datasets/origine-de-lelectricite-fournie-par-edf-sa/
<br>

production d'edf dans tous les pays ou il est <br>
https://opendata.edf.fr/explore/dataset/capacites-de-production-par-pays-du-groupe-edf/table/?disjunctive.perimetre_spatial&disjunctive.filiere&sort=-tri
<br>
https://opendata.edf.fr/explore/dataset/productions-consolidees-par-pays-du-groupe-edf/information/?disjunctive.perimetre_spatial&disjunctive.filiere&sort=-tri
<br>

données entre 1990 et 2015 <br>
https://www.theshiftdataportal.org/energy/nuclear?chart-type=line&chart-types=line&chart-types=ranking&energy-unit=%25&group-names=France&group-names=Germany&group-names=Japan&is-range=true&dimension=shareOfElectricityGeneration&end=2015&start=1990&multi=true
<br>



