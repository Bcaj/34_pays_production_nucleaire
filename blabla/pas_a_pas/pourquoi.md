# Comment faire une analyse 
## Choisir une question
Quel est la production d'énergie nucléaire mondiale, et peut-on prédire la production pour l'année suivante ?

## Chercher des données
kaggle : site qui est centré sur de l'échange d'information autour de la science des données  
https://www.kaggle.com/datasets/tariqbashir/nuclear-power-generation-by-countries  
en quelques minutes il y a un jeu de données opérationnel pour faire des maths.  

## Préparer son environment informatique
Environment d'analyse va se faire en python 3.11 sur ubuntu 22.04.03. Avec le logiciel DataSpell2023.2.1 pour avoir un IDE qui prend presque tout en charge dans un seul logiciel.
- le gestionnaire de paquet python : anaconda
- gestion de la gestion des données
- utilisation de jupyter-notebook pour faire des tests
- création de scripts python tout au long du projet pour que tout soit exécutable sur un autre environment
- fichier d'informations sur les modules python qui vont être installé, avec leurs versions
- utilisation d'un systeme de controle de version : GIT puis envoi sur GITLAB https://gitlab.com/Bcaj/34_pays_production_nucleaire

## Appliquer des modèles mathématiques informatiquement
Techniquement, on respecte la démarche théorique pour faire de l'intelligence artificielle :
Une question, des données, un ordinateur avec un environment de programmation et une connexion internet pour faire des recherches.

### Des colonnes, des lignes et des valeurs entres
Pour chaque colonne, on va estimer la prochaine valeur en modélisant une ligne passant le plus proche possible des valeurs.
Chaque variable est indépendante de l'autre dans cette hypothèse. Le résultat d'une colonne ne dépend pas d'une autre variable.

