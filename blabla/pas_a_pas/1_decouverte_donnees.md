# Un fichier csv 
10 lignes, 34 colonnes  
La 1er colonne est l'année, il y a donc déjà un premier calcul mathématique à faire : 
la soustraction, 34 - 1 = 33 : il y a 33 pays qui produisent de l'énergie nucléaire et qui acceptent de partager leurs informations de production.

## 33 pays
1 heure de projet et d'installation, et déjà 1 erreur, le titre du projet :) 

# Ouvrir le fichier et l'observer
Pandas, la librairie python qui permet de manipuler des données rapidement avec des fonctions pratiques pour faire de la modification.
numpy, faire des mathématiques avec python avec des manières plus rapide que du python pur
plotly ou matplotlib : permettent de faire des graphiques, mettre en image des nombres
## Analyse  
On peut faire une regression par colonne pour estimer le prochain résultat. 
### Faire un test en retirant la dernière valeur
Et si avec toutes les autres données présentes, on peut arriver à approximer la dernière ligne, alors il y a une piste à continuer.
## Nettoyage 
Ne garder que les noms de pays
# Une base de données
Un fichier à plat, c'est sympa, mais dans l'optique de transférer des données et de faire plus de manipulation, une base de données va être construite localement pour le projet
## SQLITE3
Avec une relation entre différente table, pouvoir utiliser les données pour les pays un par un pour faire des prédictions pays par pays.

![alt text](../presentation/1_preparation_data/2_uml.png)

# Script de création de la bdd et ajout des premières données
- lire le fichier csv
- modifier l'index
- changer le nom des colonnes
- créer les 3 tables en même temps que la base 
- insérer les données pour chaque catégorie : 
  - pays
  - années
  - données reliant les 2
