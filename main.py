import sqlite3
import donnes.creation_bdd as cbd
import pandas as pd


def main():
    # connection et création base si 1er lancement
    conn = None
    cur = None
    try:
        conn = cbd.connection('nuc')
        cbd.creation_tables(conn)
        print("connecté et base accessible avec les tables ")
        cur = conn.cursor()

    except sqlite3.Error as e:
        print(e)
        # todo : faire un fichier de log plutôt qu'un print, et changer le nom du chemin en config

    # lecture et transformation du csv
    df = transformation_csv()
    #print(df)
    # insertion dans la base de donnée
    cbd.insert_tout(cur, df)
    conn.commit()

    # fermeture avec la base
    cbd.deconnection(conn)


def transformation_csv():
    df = pd.read_csv("donnes/Nuclear Generated Energy share by Countries.csv")
    df.set_index(df.columns[0], inplace=True)
    noms_cols = df.columns.values
    pays_cols = []

    for pays in noms_cols:
        premier = pays.split(' ')
        pays_cols.append(premier[0])

    for pays in range(len(pays_cols)):
        if pays_cols[pays][:3] == 'Swe':
            pays_cols[pays] = 'sweden'
        pays_cols[pays] = pays_cols[pays].lower()

    df.columns = pays_cols
    df = df[['france', 'germany', 'japan']]

    return df


if __name__ == '__main__':
    main()
